
import { Observable, of } from 'rxjs';

import { HttpClient } from '@angular/common/http';
import { Injectable, Inject } from '@angular/core';
import { IMateria, ICuatrimestre } from './materia';
@Injectable({
  providedIn: 'root'
})
export class MateriasService {

  private apiURL = 'https://localhost:5001/' + 'api/materias';
  constructor(private http: HttpClient) { }

  getMaterias(): Observable<ICuatrimestre[]> {
    return this.http.get<ICuatrimestre[]>(this.apiURL);
  }

  setMateria(materia: IMateria): Observable<IMateria> {
    return this.http.post<IMateria>(this.apiURL, materia);
  }

  deleteMateria(materiaId: string): Observable<IMateria> {
    return this.http.delete<IMateria>(this.apiURL + '/' + materiaId);
  }
 
}
