export interface HorarioModel {
    id: number,
    idMateria: number,
    estado: boolean,
    aula: string,
    grupo: number,
    lun: string,
    mar: string,
    mier: string,
    jue: string,
    vie: string,
    sab: string,
    dom: string,
    modulo: number,
    cupo: number
}