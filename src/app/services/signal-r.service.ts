import { Injectable } from '@angular/core';
import * as signalR from "@aspnet/signalr";
import { HorarioModel } from '../models/HorarioModel';
import { IHorario } from '../horarios/horario';

@Injectable({
  providedIn: 'root'
})
export class SignalRService {
  public data: IHorario[];

  private hubConnection: signalR.HubConnection

  public registerConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl('https://localhost:5001/horarios')
      .build();
  }

  public startConnection = (callback: () => void) => {
    this.hubConnection
      .start()
      .then(() => {
        console.log('Connection started');
        callback();
      })
      .catch(err => console.log('Error while starting connection: ' + err))
  }

  public transferData = (callback: () => void) => {
    this.hubConnection.on('disponibles', (data) => {
      this.data = data;
      console.log("fromServer", data);
      callback();
    });
  }

  joinGroup(group: string): void {
    if (this.hubConnection) {
      this.hubConnection.invoke('JoinGroup', group);
    }
  }

}