import { Component, OnInit } from '@angular/core';
import { IHorario } from './horarios/horario';
import { IMateria, ICuatrimestre } from './materias/materia';
import { HorariosService } from './horarios/horarios.service';
import { MateriasService } from './materias/materias.service';
import { MateriasComponent } from './materias/materias.component';
import { HorariosComponent } from './horarios/horarios.component';
import { SignalRService } from './services/signal-r.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'matriculacion';
  public cuatrimestres: ICuatrimestre[];
  public horarios: IHorario[];
  public textoMateria: string;
  public materiaId: number;

  public horariosSeleccionado: any[] = [];



  constructor(private materiasService: MateriasService, private horariosService: HorariosService, public signalRService: SignalRService) { }
  ngOnInit() {
    this.signalRService.registerConnection();
    this.signalRService.transferData(this.retrieveHorario);
    this.signalRService.startConnection(this.cargarHorarios);
    this.cargarMaterias();
  }

  cargarHorarios = () => {
    this.horariosService.init().subscribe(data => console.log(data))
  }
  cargarMaterias() {
    this.materiasService.getMaterias()
      .subscribe(materiasWS => {
        console.log(materiasWS)
        this.cuatrimestres = materiasWS;

        this.cuatrimestres.forEach(element => {
          element.materias.forEach(x => {
            x.estado = false;
          })

        });
      },
        error => console.error('Aqui hubo un error: ', error));
  }

  pedirHorario(materia) {
    this.cuatrimestres.forEach(cuat => {
      cuat.materias.forEach(actual => {
        actual.seleccionado = (actual.id == materia.id)
      })
    })


    this.textoMateria = materia.codigo + ' - ' + materia.descripcion;
    this.materiaId = materia.id;
    this.retrieveHorario();


    this.horarios.forEach(element => {
      element.seleccionado = true;
    });

  }

  private retrieveHorario = () => {
    if (this.materiaId)
      this.horarios = this.signalRService.data.filter(x => x.idMateria == this.materiaId);
  }

  private isIncluded(horario) {
    let included = false;
    this.horariosSeleccionado.forEach(x => {
      if (x.id == horario.id)
        included = true;
    });
    return included;
  }

  private isMateriaSelected(id) {
    let selected = false;
    let cuat = this.cuatrimestres.find(x => x.cuatrimestre == id);
    if (!cuat)
      return selected;

    cuat.materias.forEach(x => {
      if (x.estado)
        selected = true;
    })

    return selected;
  }

  inscribir(horario) {

    let isNewitem = !this.isIncluded(horario);

    this.horariosService.setHorario(horario, isNewitem)
      .subscribe(res => {
        this.cuatrimestres.forEach(el => {
          el.materias.filter(x => x.id === horario.idMateria).forEach(element => {
            element.estado = !element.estado;
          });
        });
        if (isNewitem)
          this.horariosSeleccionado.push(horario);
        else {
          this.horariosSeleccionado.splice(this.horariosSeleccionado.indexOf(this.horariosSeleccionado.find(x => x.id == horario.id)), 1);
        }

        console.log("Horario seleccionado: ", this.horariosSeleccionado)
      },
        error => alert(error.message));
  }

  mostrar(mate) {
    console.log(mate);
  }

  descripcionCuatrimestre(idCuatrimestre) {
    return 'CUATRIMESTRE ' + idCuatrimestre;
  }

  validarCedula(cedula: string) {
    let pat1 = /[0-9]{11}/
    alert(pat1.test(cedula))
  }

}
